import org.opencv.core.*;
import org.opencv.core.Point;
import org.opencv.imgproc.Imgproc;
import org.opencv.videoio.VideoCapture;
import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.List;

import static org.opencv.core.CvType.*;
import static org.opencv.imgcodecs.Imgcodecs.imencode;

//java -jar -Djava.library.path=/usr/local/Cellar/opencv3/3.2.0/share/OpenCV/java out/artifacts/Recognise_Colors_jar/Recognise\ Colors.jar

public class Main {
    static {
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
    }

    public static void main(String[] args) {

        JFrame jframe = new JFrame("Red Color detector 3000");
        jframe.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        JLabel vidpanel = new JLabel();
        jframe.setContentPane(vidpanel);
        jframe.setSize(640, 480);
        jframe.setVisible(true);

        JFrame jframe2 = new JFrame("Original Video");
        jframe2.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        JLabel vidpanel2 = new JLabel();
        jframe2.setContentPane(vidpanel2);
        jframe2.setSize(640, 480);
        jframe2.setVisible(true);

        Mat frame = new Mat();
        Mat outerBox = new Mat();
        Mat lower_red_hue_range = new Mat();
        Mat upper_red_hue_range = new Mat();
        Size sz = new Size(640, 480);
        VideoCapture camera = new VideoCapture(args[0]);

        List<Pt> list = new ArrayList<Pt>();

        while (true) {
            if (camera.read(frame)) {
                Imgproc.resize(frame, frame, sz);

                outerBox = new Mat(frame.size(), CV_8UC1);
                Imgproc.cvtColor(frame, outerBox, Imgproc.COLOR_BGR2HSV);

                Core.inRange(outerBox, new Scalar(0, 100, 100), new Scalar(10, 255, 255), lower_red_hue_range);
                Core.inRange(outerBox, new Scalar(160, 100, 100), new Scalar(179, 255, 255), upper_red_hue_range);

                Mat red_image = new Mat();
                Core.addWeighted(lower_red_hue_range, 1.0, upper_red_hue_range, 1.0, 0.0, red_image);
                Imgproc.GaussianBlur(red_image, red_image, new Size(9, 9), 2, 2);

                java.util.List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
                Imgproc.findContours(red_image, contours, new Mat(), Imgproc.RETR_LIST, Imgproc.CHAIN_APPROX_SIMPLE);

                for (int i = 0; i < contours.size(); i++) {
                    if (Imgproc.contourArea(contours.get(i)) > 50 ) {
                        Rect rect = Imgproc.boundingRect(contours.get(i));

                        if (rect.height > 28) {
                            Imgproc.rectangle(frame, new Point(rect.x,rect.y), new Point(rect.x+rect.width,rect.y+rect.height), new Scalar(0,0,255));

                            int x = rect.x + (rect.width / 2);
                            int y = rect.y + (rect.height / 2);


                            Pt p = new Pt(x, y);
                            list.add(p);
                        }
                    }
                }

                for(Pt b:list){
                    Imgproc.circle(frame, new Point(b.x, b.y),10, new Scalar(0,0,255), 18);
                }

                ImageIcon image2 = new ImageIcon(Mat2bufferedImage(frame));
                vidpanel2.setIcon(image2);
                vidpanel2.repaint();

                ImageIcon image = new ImageIcon(Mat2bufferedImage(red_image));
                vidpanel.setIcon(image);
                vidpanel.repaint(); 
            }
        }
    }

    private static BufferedImage Mat2bufferedImage(Mat image) {
        MatOfByte bytemat = new MatOfByte();
        imencode(".jpg", image, bytemat);
        byte[] bytes = bytemat.toArray();
        InputStream in = new ByteArrayInputStream(bytes);
        BufferedImage img = null;
        try {
            img = ImageIO.read(in);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return img;
    }
}

class Pt {
    double x, y;

    public Pt(double x, double y) {
        this.x = x;
        this.y = y;
    }
}